import plotly.graph_objects as go

if __name__ == '__main__':
    traces = list()
    traces.append(dict(type='scatter', x=[1, 2, 3, 4], y=[5, 6, 7, 8], name='mean'))
    dict_of_fig = dict({'data': traces, 'layout': {"title": {"text": "My Diagram"}}})
    fig = go.Figure(dict_of_fig)
    fig.show()

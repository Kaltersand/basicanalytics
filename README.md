# README

This is a base project for analytics.

## Installation steps
    Install a clean Python385 interpreter (64bit) on C:\ (if not existent)

    Clone repository
        https://Kaltersand@bitbucket.org/Kaltersand/basicanalytics.git

    Install virtual environment
        C:\python385\scripts\pip.exe install virtualenv

    Create virtual environment for swimstats
        Go to project root
        C:\python385\scripts\virtualenv.exe -p C:\python385\python.exe env

    Install required Python packages
        Go to project root
        .\env\scripts\pip install -r requirements.txt

## Run the Application
    Execute
        either with
            .\env\Scripts\activate.bat
            jupiter notebook
        or simply
            execute jupyter_start.bat

## Dealing with Python Packages
    Installing and uninstalling modules
        .\env\Scripts\pip install package_x
        .\env\Scripts\pip uninstall package_x
        .\env\Scripts\pip install --upgrade package_x

        .\env\Scripts\pip freeze > requirements.txt
        .\env\Scripts\pip install -r requirements.txt

RoA 23.12.2020